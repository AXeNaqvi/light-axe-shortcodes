#Light AXe Shortcodes

**Contributors:** axenaqvi

**Theme URI:** [https://bitbucket.org/AXeNaqvi/light-axe-shortcodes/](https://bitbucket.org/AXeNaqvi/light-axe-shortcodes/)

**Author:** AZ Naqvi

**Author URI:** [https://bitbucket.org/AXeNaqvi/](https://bitbucket.org/AXeNaqvi/)

**Version:** 1.0.6

**Stable tag:** 1.0.0

**Requires at least:** Wordpress 4.7

**Tested up to:** Wordpress 5.3.2

**Text Domain:** light-axe-shortcodes

**License:** GNU General Public License v2.0 and above

**License URI:** http://www.gnu.org/licenses/gpl-2.0.html

**Description:** Contains shortcodes and widgets for theme Light AXe.


##Description

This plugin adds shortcodes and widgets to be used with theme Light AXe and its child themes.

##Changelog:
---
###Ver 1.0.6
1. Tested upto WP 5.3.2
2. Added new filter for adbox content order array.

###Ver 1.0.5
1. Tested upto WP 5.2.4
2. Added new container blocks for each axeadbox item.

###Ver 1.0.4
1. Tested upto WP 5.2.3
2. Added copyright year shortcode. Easy to set and change per year basis on its own.
3. Added titles for woocommerce cart shortcode.

###Ver 1.0.3
1. Tested upto WP 5.2.2
2. Added support to remove target from local links in sociallinks module.

###Ver 1.0.2
1. Tested upto WP 5.1.1
2. Added full block anchor for default post type of carousel.

###Ver 1.0.1
1. Fixed predefied layouts in widgets.
2. Added support for scrolling widgets with sidebox shortcode.

###Ver 1.0.0
1. First version.
2. Ready to be uploaded to WP Plugins Directory.