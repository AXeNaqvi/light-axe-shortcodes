<?php

if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}


/* AXe Map */
function themeaxe_Map($atts){
	$gmaps = themeaxe_GetAllThemeSettings('gmaps');
	$apikey = $gmaps->gmaps->value;
	$atts = shortcode_atts(
		array(
			'width'=>350,
			'height'=>250,
			'location'=> themeaxe_getDefaultAddress(),
		),$atts
	);

	$html = '';
	if(!empty($apikey)){
		$html = '<iframe width="'.$atts['width'].'"
		height="'.$atts['height'].'"
		frameborder="0"
		style="border:0"
		src="https://www.google.com/maps/embed/v1/place?key='.$apikey.'&q='.$atts['location'].'">
		</iframe>';
	}

	return $html;
}
add_shortcode('axeGmap','themeaxe_Map');
/* AXe Map */

function themeaxe_getDefaultAddress($joinedby = ' '){
	$contact = themeaxe_GetAllThemeSettings('contact');
	$address = $contact->address->value;
	$address2 = $contact->address2->value;
	$location = empty($address2) ? $address : $address . $joinedby. $address2;

	return $location;
}

function themeaxe_EmbedMap($atts){
	$atts = shortcode_atts(
		array(
			'src' => '',
			'width' => '100%;',
			'height' => '250'
		),$atts);

	extract($atts);

	$html = '<iframe src="https://www.google.com/maps/embed?pb='.urlencode($src).'" width="'.$width.'" height="'.$height.'" frameborder="0" style="border:0" allowfullscreen></iframe>';

	return $html;
}
add_shortcode('embedmap','themeaxe_EmbedMap');

?>