<?php

if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}


/* Vimeo */
function themeaxe_Vimeo($atts){
	$atts = shortcode_atts(array(
		'video'=>'18249980',
		'color' => 'da251d',
		'width'=>500,
		'height'=>280
		),$atts);

	$html = '';
	$html .= '<iframe src="//player.vimeo.com/video/'.$atts['video'].'?color='.$atts['color'].'" width="'.$atts['width'].'" height="'.$atts['height'].'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="max-width:100%;"></iframe>';
	return $html;
}
add_shortcode('vimeo','themeaxe_Vimeo');
/* Vimeo */

/* Youtube */
function themeaxe_Youtube($atts){
	$atts = shortcode_atts(array(
		'video'=>'-kkEj4rI2t8',
		'width'=>500,
		'height'=>280,
		'showrelated'=>false
		),$atts);

	$related = '?rel=0';
	if($atts['showrelated']){
		$related = '';
	}

	$html = '';
	$html .= '<iframe width="'.$atts['width'].'" height="'.$atts['height'].'" src="https://www.youtube.com/embed/'.$atts['video'].$related.'" frameborder="0" allowfullscreen></iframe>';
	return $html;
}
add_shortcode('youtube','themeaxe_Youtube');
/* Youtube */

/* Dailymotion */
function themeaxe_Dailymotion($atts){
	$atts = shortcode_atts(array(
		'video'=>'x11c5z6',
		'width'=>500,
		'height'=>280
		),$atts);

	$related = '?rel=0';
	if($atts['showrelated']){
		$related = '';
	}

	$html = '';
	$html .= '<iframe frameborder="0" width="'.$atts['width'].'" height="'.$atts['height'].'" src="//www.dailymotion.com/embed/video/'.$atts['video'].'" allowfullscreen></iframe>';
	return $html;
}
add_shortcode('dailymotion','themeaxe_Dailymotion');
/* Dailymotion */

?>