<?php

if (!defined('THEMEAXESHORTCODES')) {
    exit('What are you doing here??');
}

function themeaxe_bigbox($attr, $content)
{
    $attr = shortcode_atts(array(
        'id' => '',
        'class' => '',
    ), $attr);

    $attr['id'] = empty($attr['id']) ? '' : 'id="' . $attr['id'] . '"';

    $html = '<div ' . $attr['id'] . ' class="axebigbox ' . $attr['class'] . '">';
    $html .= themeaxe_ContentFilterNoWPAutoP($content);
    $html .= '</div>';

    return $html;
}
add_shortcode('axebigbox', 'themeaxe_bigbox');

function themeaxe_box($attr, $content)
{
    $attr = shortcode_atts(array(
        'id' => '',
        'class' => '',
    ), $attr);

    $id = empty(trim($attr['id'])) ? '' : trim($attr['id']);
    $attr['id'] = empty($id) ? '' : 'id="' . $attr['id'] . '"';
    $html = '<div ' . $attr['id'] . ' class="axebox axebox_' . $id . ' ' . $attr['class'] . '">';
    $html .= themeaxe_ContentFilterNoWPAutoP($content);
    $html .= '</div>';
    return $html;
}
add_shortcode('axebox', 'themeaxe_box');

function themeaxe_br()
{
    return '<br/>';
}
add_shortcode('axebr', 'themeaxe_br');

/* Ad Box */
function themeaxe_AdBox($atts)
{
    $atts = shortcode_atts(array(
        'title' => '',
        'subtitle' => '',
        'img' => '',
        'url' => '',
        'class' => '',
        'newwindow' => 0,
        'readmore' => 0,
        'text' => '',
        'readmoretext' => 'Read More',
        'predefinedlayout' => 'ihc',
    ), $atts);
    $usedefaults = class_exists('SiteOrigin_Panels') ? false : true;
    $html = '';
    $img = '';
    $readmore = '';
    $class = trim($atts['class']);
    $newwin = intval($atts['newwindow']) ? 'target="_blank"' : '';
    if (!empty($atts['img'])) {
        $sizes = themeaxe_GetImageSize($atts['img']);
        $img .= '<div class="adboxlayoutblocks adboximgblock"><div class="adboximg"><img src="' . $atts['img'] . '" alt="' . $atts['title'] . '" ' . $sizes . '/></div></div>';
    }
    $text = '';
    remove_filter('the_content', 'wpautop');
    if (!empty($atts['text'])) {
        $text = $usedefaults ? wpautop(htmlspecialchars_decode(apply_filters('the_content', $atts['text']))) : $atts['text'];
        $text = '<div class="adboxlayoutblocks adboxexcerptblock"><div class="adboxexcerpt">' . $text . '</div></div>';
    }
    $heading = '';
    if (!empty($atts['title'])) {
        $heading = $usedefaults ? apply_filters('the_content', $atts['title']) : $atts['title'];
        $heading = '<div class="adboxlayoutblocks adboxtitleblock"><h3 class="adboxtitle">' . $heading . '</h3></div>';
    }
    if (!empty($atts['subtitle'])) {
        $subtitle = $usedefaults ? apply_filters('the_content', $atts['subtitle']) : $atts['subtitle'];
        $heading .= '<div class="adboxlayoutblocks adboxsubtitleblock"><h4 class="adboxsubtitle">' . $subtitle . '</h4></div>';
    }
    add_filter('the_content', 'wpautop');
    $order = themeaxe_getPredefinedLayout($atts['predefinedlayout']);
    $order['i'] = $img;
    $order['h'] = $heading;
    $order['c'] = $text;
    $order = apply_filters('axe_adbox_content_order_array_filter', $order, $atts);
    $content = '';
    foreach ($order as $k => $v) {
        $content .= $v;
    }

    $idsearch = array('/\s+/', '/[^a-zA-Z0-9\-]+/', '/(\-)+/');
    $idreplace = array('-', '', '-');
    $adid = preg_replace($idsearch, $idreplace, strtolower($atts['title']));

    if (intval($atts['readmore'])) {
        $readmore = '<a class="adboxreadmore" ' . $newwin . ' href="' . $atts['url'] . '" title="' . $atts['title'] . '">' . $atts['readmoretext'] . '</a>';

        $html .= '<div id="adbox-' . $adid . '" class="axeadbox ' . $class . '">' . $content . $readmore . '</div>';
    } else {
        if ($atts['url'] && $atts['url'] != '#') {
            $html .= '<div id="adbox-' . $adid . '" class="axeadbox ' . $class . '"><a class="axeadboxwrapperlink" ' . $newwin . ' href="' . $atts['url'] . '" title="' . $atts['title'] . '">' . $content . '</a>' . $readmore . '</div>';
        } else {
            $html .= '<div id="adbox-' . $adid . '" class="axeadbox ' . $class . '">' . $content . '' . $readmore . '</div>';
        }
    }

    return $html;
}
add_shortcode('axeadbox', 'themeaxe_AdBox');
/* Ad Box */

function themeaxe_sidebBoxCaller($atts)
{
    $atts = shortcode_atts(array(
        'id' => '',
        'class' => '',
        'withscroller' => 0,
        'mode' => 'horizontal',
        'in' => 800,
        'out' => 800,
        'time' => 8000,
        'autoplay' => 0,
        'ezing' => 'linear',
        'itemstoshow' => 1,
        'dir' => 'norm',
        'containerclass' => 'axescrollercontainer',
        'childclass' => 'widget',
        'post_type' => 'widgets',
        'playpausebutton' => 0,
    ), $atts);
    $html = '';
    if (!empty($atts['id'])) {
        if (intval($atts['withscroller']) == 1) {
            $container = $atts['containerclass'];
            $childclass = $atts['childclass'];
            $html .= '<div id="' . $atts['id'] . '" class="top' . $atts['post_type'] . '-sidebar  top' . $atts['post_type'] . '-carousel myclass axeCarousel ' . $atts['post_type'] . 'axeCarousel mode' . $atts['mode'] . ' '.$atts['class'].'">';
            $html .= '<a href="#" class="axecarouselarrows axecarouselnext">&lt;</a>';
            if (intval($atts['playpausebutton']) == 1) {
                $html .= '<a href="#" class="axecarouselarrows axecarouselplaypause fa fa-pause"></a>';
            }
            $html .= '<a href="#" class="axecarouselarrows axecarouselback">&gt;</a>';
            $html .= '<div class="ohidden">';
            $html .= '<div class="' . $container . ' myinnerclass carouselcontainer">';

            $html .= themeaxe_get_sidebar($atts['id']);

            $html .= '</div><!-- ' . $container . ' ends -->';
            $html .= '</div><!-- ohidden ends -->';
            $html .= '</div>';
            $html .= '<script> jQuery(document).ready(function($){ $(\'#' . $atts['id'] . '\').axeCarousel({childcontainer:\'' . $container . '\',childclass:\'' . $childclass . '\',mode:\'' . $atts['mode'] . '\',i:' . $atts['in'] . ',t:' . $atts['time'] . ',o:' . $atts['out'] . ',autoplay:' . $atts['autoplay'] . ',dir:\'' . $atts['dir'] . '\',';
            if ($atts['post_type'] == 'axe_gallery') {
                $html .= 'showbigimg:1,';
            }
            if (isset($atts['itemstoshow'])) {
                $html .= 'toshow:' . intval($atts['itemstoshow']) . ',';
            }
            $html .= 'ezing:\'' . $atts['ezing'] . '\'}); }); </script>';

        } else {
            $html .= '<div id="' . $atts['id'] . '" class="axesidebox ' .$atts['id'] . ' ' . $atts['class'] . '">' . themeaxe_get_sidebar($atts['id']) . '</div>';
        }
    }
    return $html;
}
add_shortcode('sidebox', 'themeaxe_sidebBoxCaller');

/* Page Excerpt Boxes */
function themeaxe_PageExcerpts($atts)
{
    $atts = shortcode_atts(
        array(
            'pageids' => '',
            'class' => '',
        ),
        $atts);
    $pageids = explode(',', $atts['pageids']);
    if ($pageids) {
        $query = themeaxe_GetPageList(array('post__in' => $pageids));

        $html = '';

        if ($query->have_posts()) {
            $html .= '<div class="pageexcerpt ' . $atts['class'] . '">';
            $i = 1;
            $total = $query->post_count;
            while ($query->have_posts()) {
                $query->the_post();
                $url = get_the_permalink();
                $excerpt = get_post_meta(get_the_ID(), 'page_excerpt', true);
                if ($i % 2 == 1) {
                    $html .= '<div class="whitegraybg excerptouterbox">';
                }
                $html .= '<div class="pageexcerptbox">';
                $html .= themeaxe_getFeaturedImg(0, '');
                $html .= '<h3 class="excertheading">' . get_the_title() . '</h3>';
                $html .= '<div class="excerptcontent">' . $excerpt . '</div>';
                $html .= '<a href="' . $url . '" class="readmore">Read More</a>';
                $html .= '';
                $html .= '</div>';
                if ($i % 2 == 0) {
                    $html .= '';
                    $html .= '</div>';
                } else if ($i == $total) {
                    $html .= '';
                    $html .= '</div>';
                }
                $i++;
            }
            $html .= '';
            $html .= '</div>';
        }
        wp_reset_postdata();
        return $html;
    }
}
add_shortcode('axepageexcerpts', 'themeaxe_PageExcerpts');
/* Page Excerpt Boxes */

/* Before Footer Sections */
function themeaxe_innerpageBottomSidebar()
{?>

</div>

<?php
global $post;
if (!$post) {return;}
$sidebars = get_post_meta($post->ID, 'before_footer', false);
if ($sidebars) {
    foreach ($sidebars as $sidebar) {
        $sbar = explode(',', $sidebar);
        if (!isset($sbar[1])) {
            $sbar[1] = 'blue';
        }
        ?>
        <div class="beforebottomsection">
           <div class="widebox <?php echo $sbar[1]; ?>">
            <div class="wideboxinnerwrapper">
             <?php themeaxe_sidebar($sbar[0]);?>

         </div>

     </div>

 </div>


 <?php
}
}
?>

<div>
    <?php

}
add_action('before_axe_get_footer', 'themeaxe_innerpageBottomSidebar', 999);
/* Before Footer Sections */

/* Left Sidebar Per Page */
function themeaxe_LeftSidebars()
{
    global $post;
    $sidebars = get_post_meta($post->ID, 'left_sidebar', false);
    if ($sidebars) {
        foreach ($sidebars as $sidebar) {
            ?>
            <div class="customleftsidebar">
                <?php themeaxe_sidebar($sidebar);?>

            </div>
            <?php
        }
    } else {
        themeaxe_sidebar('leftsidebar', true);
    }
}
add_action('left_sidebar_caller', 'themeaxe_LeftSidebars');
/* Left Sidebar Per Page */

/* Right Sidebar Per Page */
function themeaxe_RightSidebars()
{
    global $post;
    $sidebars = get_post_meta($post->ID, 'right_sidebar', false);
    if ($sidebars) {
        foreach ($sidebars as $sidebar) {
            ?>
            <div class="customrightsidebar">
                <?php themeaxe_sidebar($sidebar);?>

            </div>
            <?php
        }
    } else {
        themeaxe_sidebar('rightsidebar', true);
    }
}
add_action('right_sidebar_caller', 'themeaxe_RightSidebars');
/* Right SIdebar Per Page */
?>