<?php

if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}


/* Visiting Card */
function themeaxe_VisitingCard($atts){
	$address = themeaxe_GetAllThemeSettings('contact');

	$def = apply_filters('filter_vcardshortcode_atts',array(
		'showthese' => 'address,phone1,phone2,fax,email,officehours',
		'address' => themeaxe_getDefaultAddress('<br/>'),
		'phone1' => $address->phone1->value,
		'phone2' => $address->phone2->value,
		'fax' => $address->fax->value,
		'email' => $address->email->value,
		'officehours' => $address->officehours->value,
		'usedefaults' => 0
	),$address);

	$atts = shortcode_atts($def,$atts);



	$showthese = explode(',', $atts['showthese']);

	$allfields = apply_filters('filter_vcardshortcode_allfields',array(
		'address' => array('label'=>__('Address:', 'light-axe'),'type'=>'address','nl2br'=>false),
		'phone1' => array('label'=>__('Phone:', 'light-axe'),'type'=>'phone','nl2br'=>false),
		'phone2' => array('label'=>__('Phone 2:', 'light-axe'),'type'=>'phone','nl2br'=>false),
		'fax' => array('label'=>__('Fax:', 'light-axe'),'type'=>'fax','nl2br'=>false),
		'email' => array('label'=>__('Email:', 'light-axe'),'type'=>'email','nl2br'=>false),
		'officehours' => array('label'=>__('Office Hours:', 'light-axe'),'type'=>'officehours','nl2br'=>true),
	));

	$html = '<div class="axevcard">';
	$html .= '<ul>';
	$args = array('key'=>'');

	foreach($showthese as $sthese){
		$args['key'] = $sthese;
		if($atts['usedefaults']){
			if($sthese == 'address'){
				$atts[$sthese] = themeaxe_getDefaultAddress('<br/>');
			}else{
				$atts[$sthese] = $address->$sthese->value;
			}
		}
		$arrthese = isset($allfields[$sthese]) && is_array($allfields[$sthese]) ? $allfields[$sthese] : array('label'=>'','type'=>'','nl2br'=>false);
		if(!empty($atts[$sthese])){
			$val = $atts[$sthese];
			if($arrthese['nl2br'] === true){
				$val = nl2br($val);
			}
			$html .= '<li class="vcard'.$sthese.'"><span class="vcardlabel">'. apply_filters('vcard_label_filter',$arrthese['label'],$arrthese['type']) .' </span>'.apply_filters('vcard_value_filter',$val,$arrthese['type']).'</li>';
		}
	}

	$html .= '</ul>';
	$html .= '';
	$html .= '</div>';
	return $html;
}
add_shortcode('axevcard','themeaxe_VisitingCard');

function themeaxe_getVCardValues($atts){
	$atts = (shortcode_atts(array(
		'key'=>'',
	),$atts));
	$key = $atts['key'];

	$address = themeaxe_GetAllThemeSettings('contact');

	if(isset($address->$key)){
		return $address->$key->value;
	}else{
		return $key;
	}
}
add_shortcode('vcardvalue','themeaxe_getVCardValues');
/* Visiting Card */

?>