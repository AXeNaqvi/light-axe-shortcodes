<?php

if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}

function themeaxe_axebutton($atts, $content) {
	$atts = shortcode_atts(
		array(
			'text'=>__('Click Here', 'light-axe'),
			'link'=>'#',
			'target'=>'_self',
			'class'=>'',
			'id'=>'',
			'data'=>'',
			'title' => ''
		),
		$atts);
	$html = '';
	$html .= '<a id="'.trim($atts['id']).'" title="'.trim($atts['title']).'" href="'.trim($atts['link']).'" data="'.trim($atts['data']).'" target="'.trim($atts['target']).'" class="button axebutton '.trim($atts['class']).'">'.trim($atts['text']).'</a>';
	return $html;
}
add_shortcode ( 'axebutton', 'themeaxe_axebutton' );
?>