<?php

if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}

function themeaxe_Copyrightyear($atts){
		$atts = extract(shortcode_atts(
			array(
				'startyear' => date('Y'),
			),$atts
		));
		$startyear = intval($startyear);
		$startyear = $startyear >= 1800 ? $startyear : date('Y');
		if($startyear >= date('Y')){
			return date('Y');
		}else{
			return $startyear . ' - ' . date('Y');
		}

	}
add_shortcode ( 'axeCopyrightyear', 'themeaxe_Copyrightyear' );

?>