<?php

if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}


/* WooCommerce Cart */

function themeaxe_WooCart($atts){
	$atts = shortcode_atts(array(
		'showaccount'=>1,
		'accounttxt' => 'My Account',
		'carttxt' => 'Shopping Cart',
		),$atts);
	$html = '';
	$account =  get_option('woocommerce_myaccount_page_id');
	if(intval($account)){
		$html .= '<ul class="axewoocart">';
		if(intval($atts['showaccount'])){
			$html .= '<li class="liaxeaccount"><a href="'.get_permalink($account).'" title="'.$atts['accounttxt'].'">'.apply_filters('axewoocartaccounttext_filter',$atts['accounttxt']).'</a></li>';
		}
		$cart =  get_option('woocommerce_cart_page_id');
		global $woocommerce;
		$count = 0;
		if($woocommerce){
			$count = $woocommerce->cart->cart_contents_count;
			$count = apply_filters('axewoocartcount_filter',$count);
		}
		$html .= '<li class="liaxecart"><a href="'.get_permalink($cart).'" title="'.$atts['carttxt'].'">'. apply_filters('axewoocarttext_filter',$atts['carttxt']).' <span class="axecartcount">'.$count.'</span></a></li>';
		$html .= '</ul>';
	}
	return $html;
}
add_shortcode('axewoocart','themeaxe_WooCart');

/* WooCommerce Cart */

?>