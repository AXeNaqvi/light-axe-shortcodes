<?php

if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}

function themeaxe_SocialLinks($attr) {
	$attr = shortcode_atts ( array (
		'class' => '',
		'usefonts' =>''
	), $attr );

	$social = themeaxe_GetAllThemeSettings('socialmedia');
	$html = '';
	$html .= '<ul class="themeaxe-socialmedia-links '.$attr['class'].'">';
	foreach($social as $key => $val){
		$itemclass = '';
		$icon = empty($val[2]) ? $val[0] : '<img src="'.$val[2].'" />';

		if($val[4] == 'yes' || $attr['usefonts'] == 'yes'){
			$icon = '<span class="fab '.$val[3].'"></span>';
		}
		$itemclass = trim($val[5]);
		$target = substr_count($val[1], home_url()) ? '' : 'target="_blank"';
		$html .= '<li class="themeaxe-socialmedia-item '.$itemclass.'"><a href="'.$val[1].'" '.$target.' title="'.$val[0].'">'.$icon.'</a></li>';
	}
	$html .= '</ul>';

	return $html;
}
add_shortcode ( 'axeSocial', 'themeaxe_SocialLinks' );

?>