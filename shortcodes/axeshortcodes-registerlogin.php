<?php

if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}


/* Login /registration Menu */

function themeaxe_LoginMenu($atts){
	$atts = extract(shortcode_atts(array(
		'login'=>0,
		'registration'=>0
		),$atts));
	$html = '<ul class="axeloginmenu">';

	if(!is_user_logged_in()){
		$lr = get_option('woocommerce_myaccount_page_id');
		if($lr){
			$html .='<li class="axewoologinlink"><a href="'.get_permalink( $lr).'" title="'. __('Login / Register', 'light-axe') .'">'. __('Login / Register', 'light-axe') .'</a></li>';
		}else if(intval($login)){
			$html .= '<li class="axecustomloginlink"><a href="'.get_permalink($login).'">'. __('Login', 'light-axe') .'</a></li>';
			if(intval($registration)){
				$html .= '<li class="axecustomregisterlink"><a href="'.get_permalink($registration).'">'. __('Register', 'light-axe') .'</a></li>';
			}else{
				$html .= wp_register('<li class="axewpregisterlink">','</li>', false );
			}
		}
		else{
			global $user;
			$redirectto = apply_filters('axeredirectto',$_SERVER['REQUEST_URI'],$user);
			$calledfrom = '';
			if(isset($_SERVER['REQUEST_URI'])){
				$calledfrom = $_SERVER['REQUEST_URI'];
			}
			$html .= '<li class="axeloginlink">'.wp_loginout($redirectto, false ).'</li>';
			$html .= wp_register('<li class="axeregisterlink">','</li>', false );
		}
	}else{
		global $user;
		$html .= '<li class="axelogoutlink">'.wp_loginout(apply_filters('login_redirect',$_SERVER['REQUEST_URI'],$_SERVER['REQUEST_URI'],$user), false ).'</li>';
	}
	$html .= '</ul>';
	return $html;
}
add_shortcode('axeloginmenu','themeaxe_LoginMenu');

/* Login/Registration Menu */

?>