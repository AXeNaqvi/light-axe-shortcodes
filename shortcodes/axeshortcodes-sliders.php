<?php

if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}

/* AXe Carousel Image Slider */

function themeaxe_CarouselImageSlider($atts){
	$atts = shortcode_atts(array(
		'post_type' => 'axe_gallery',
		'post_status' => 'publish',
		'pagination' => false,
		'posts_per_page' => '10',
		'order' => 'DESC',
		'orderby' => 'date',
		'id'=>'topgallery-carousel-1',
		'mode'=>'horizontal',
		'in'=>800,
		'out'=>800,
		'time'=>8000,
		'autoplay'=>1,
		'ezing'=>'linear',
		'albumid'=>0,
		'template'=>'default',
		'predefinedlayout'=>'ihc',
		'itemstoshow'=>3,
		'dir' => 'norm',
		'usescroller'=>1,
		'playpausebutton' => 0
	),$atts);

	$atts['post_type'] = 'axe_gallery';

	if(intval($atts['albumid'])){
		$atts['tax_query'] = array(
			array(
				'taxonomy' => 'axe_album',
				'field' => 'id',
				'terms' => $atts['albumid']
			)
		);
	}

	$container = 'galleryitemscontainer';
	$childclass = 'topgallery-item';
	$query = themeaxe_GetPosts($atts);

	// The Loop
	$html = '';
	$html .= themeaxe_CarouselImageSliderHtml($query,$atts,$container,$childclass);
	return $html;
}
add_shortcode('axecarouselimageslider','themeaxe_CarouselImageSlider');

/* AXe Carousel Image Slider */

/* AXe Post Slider */

function themeaxe_PostSlider($atts){
	$atts = shortcode_atts(array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'pagination' => false,
		'posts_per_page' => '5',
		'order' => 'DESC',
		'orderby' => 'date',
		'id'=>'toppost-carousel-1',
		'mode'=>'horizontal',
		'in'=>800,
		'out'=>800,
		'time'=>5000,
		'autoplay'=>0,
		'ezing'=>'linear',
		'cat'=>'',
		'template'=>'default',
		'predefinedlayout'=>'ihc',
		'itemstoshow'=>3,
		'dir'=>'norm',
		'usescroller'=>1,
		'playpausebutton' => 0
	),$atts);

	$atts['post_type'] = 'post';

	/*if(!empty($atts['cat'])){
		$atts['cat'] = explode(',', $atts['cat']);
		if(!is_null($atts['cat'])){
			$atts['tax_query'] = array(
				array(
					'taxonomy' => 'category',
					'field' => 'id',
					'terms' => $atts['cat']
					)
				);
		}
	}*/

	$container = 'postitemscontainer';
	$childclass = 'topposts-item';
	$query = themeaxe_GetPosts($atts);

	// The Loop
	$html = '';
	$html .= themeaxe_PostSliderLoop($query,$atts,$container,$childclass);
	return $html;
}
add_shortcode('axepostslider','themeaxe_PostSlider');

/* AXe Post Slider */

/* AXe Page Slider */
function themeaxe_PageSlider($atts){
	$atts = shortcode_atts(
		array(
			'pageids'   => '',
			'id'=>'toppages-carousel-1',
			'mode'=>'horizontal',
			'in'=>800,
			'out'=>800,
			'time'=>5000,
			'autoplay'=>0,
			'ezing'=>'linear',
			'template'=>'default',
			'predefinedlayout'=>'ihc',
			'itemstoshow'=>3,
			'dir' => 'norm',
			'usescroller'=>1,
			'playpausebutton' => 0
		),
		$atts);
	$pageids = explode(',',$atts['pageids']);

	if($pageids){
		$query = themeaxe_GetPageList(array('post__in'=>$pageids));
		/*var_dump($query);*/
		$container = 'pageitemscontainer';
		$childclass = 'toppages-item';

		/*$container = 'productitemscontainer';
		$childclass = 'topproducts-item';*/
		$atts['post_type'] = 'page';
		$html = '';
		$html .= themeaxe_PostSliderLoop($query,$atts,$container,$childclass);
		return $html;
	}
}
add_shortcode('axepageslider','themeaxe_PageSlider');
/* AXe Page Slider */

/* AXe Product Slider */

function themeaxe_ProductSlider($atts){
	$atts = shortcode_atts(array(
		'post_type' => 'product',
		'post_status' => 'publish',
		'pagination' => false,
		'posts_per_page' => '5',
		'order' => 'DESC',
		'orderby' => 'date',
		'id'=>'topproduct-carousel-1',
		'mode'=>'horizontal',
		'in'=>800,
		'out'=>800,
		'time'=>5000,
		'autoplay'=>0,
		'ezing'=>'linear',
		'featured'=>0,
		'pcat'=>'',
		'template'=>'default',
		'predefinedlayout'=>'ihc',
		'itemstoshow'=>3,
		'dir' => 'norm',
		'usescroller'=>1,
		'playpausebutton' => 0
	),$atts);

	$atts['post_type'] = 'product';

	if(intval($atts['featured'])){
		$atts['meta_key' ] = '_featured';
		$atts['meta_value'] = 'yes';
	}

	if(!empty($atts['pcat'])){
		$atts['pcat'] = explode(',', $atts['pcat']);
		if(!is_null($atts['pcat'])){
			$atts['tax_query'] = array(
				array(
					'taxonomy' => 'product_cat',
					'field' => 'id',
					'terms' => $atts['pcat']
				)
			);
		}
	}

	$container = 'productitemscontainer';
	$childclass = 'topproducts-item';
	$query = themeaxe_GetPosts($atts);

	$html = '';
	$html .= themeaxe_PostSliderLoop($query,$atts,$container,$childclass);
	return $html;
}
add_shortcode('axeproductslider','themeaxe_ProductSlider');

/* AXe Product Slider */

/* AXe Post Template */
function themeaxe_PostSliderLoop($query,$atts,$container,$childclass){

	$html = '';
	if ( $query->have_posts() ) {
		$html .= '<div id="'.$atts['id'].'" class="top'.$atts['post_type'].'-sidebar  top'.$atts['post_type'].'-carousel myclass axeCarousel '.$atts['post_type'].'axeCarousel mode'.$atts['mode'].'">';
		if(intval($atts['usescroller'])){
			$html .= '<a href="#" class="axecarouselarrows axecarouselnext">&lt;</a>';
			if(intval($atts['playpausebutton']) == 1){
				$html .= '<a href="#" class="axecarouselarrows axecarouselplaypause fa fa-pause"></a>';
			}
			$html .= '<a href="#" class="axecarouselarrows axecarouselback">&gt;</a>';
		}
		$html .= '<div class="ohidden">';
		$html .= '<div class="'.$container.' myinnerclass '.$atts['post_type'].'carouselcontainer">';
		if($atts['template'] != 'default' && function_exists('themeaxe_carouseltemplate_'.$atts['template'])){
			$html .= call_user_func('themeaxe_carouseltemplate_'.$atts['template'],$query,$childclass, $atts);
		}else if(function_exists('themeaxe_'.$atts['post_type'].'SliderDefaultTemplate')){
			$html .= call_user_func('themeaxe_' . $atts['post_type'].'SliderDefaultTemplate',$query,$childclass,$atts);
		}else{
			$html .= themeaxe_SliderDefaultTemplate($query,$childclass);
		}
		$html .= '</div><!-- '.$container.' ends -->';
		$html .= '</div><!-- ohidden ends -->';
		$html .= '</div>';
		if(intval($atts['usescroller'])){
			$html .= '<script> jQuery(document).ready(function($){ $(\'#'.$atts['id'].'\').axeCarousel({childcontainer:\''.$container.'\',childclass:\''.$childclass.'\',mode:\''.$atts['mode'].'\',i:'.$atts['in'].',t:'.$atts['time'].',o:'.$atts['out'].',autoplay:'.$atts['autoplay'].',dir:\''.$atts['dir'].'\',';
			if($atts['post_type'] == 'axe_gallery'){
				$html .= 'showbigimg:1,';
			}
			if(isset($atts['itemstoshow'])){
				$html .= 'toshow:'.intval($atts['itemstoshow']).',';
			}
			$html .= 'ezing:\''.$atts['ezing'].'\'}); }); </script>';
		}
	} else {
		$html = 'No Items Found ...';
	}

	wp_reset_postdata();

	return $html;
}

function themeaxe_SliderDefaultTemplate($query,$childclass,$atts = null){
	$html = '';
	$order = themeaxe_getPredefinedLayout($atts['predefinedlayout']);
	while ( $query->have_posts() ) {
		$query->the_post();
		$html .= '<div class="defaultcarouselitem axecarouselitem defaultcarouseldefaulttemplateitem '.$childclass.'">';
		$url = get_the_permalink();
		$title = get_the_title();

		$heading = '<a href="'.$url.'" class="fullblockanchor" title="'.$title.'"></a><a href="'.$url.'"><h3 class="axetransition">'.$title.'</h3></a>';
		$thumb = '<a href="'.$url.'">'. getAxeFeaturedImg(0,'axetransition').'</a>';
		$text = '<p>'.get_the_excerpt().'</p>';

		$order['i'] = $thumb;
		$order['h'] = $heading;
		$order['c'] = $text;
		$content = '';
		foreach($order as $k => $v){
			$content .= $v;
		}

		/*$html .= '<div class="w w12">'.$content.'</div>';*/

		$html .= '<div class="galleryexcerpt w w12">'.$content.'</div>';
		$html .= '';
		$html .= '</div>';
	}
	return $html;
}

function themeaxe_postSliderDefaultTemplate($query,$childclass, $atts = null){
	$html = '';
	$order = themeaxe_getPredefinedLayout($atts['predefinedlayout']);
	while ( $query->have_posts() ) {
		$query->the_post();
		$html .= '<div class="postcarouselitem axecarouselitem postcarouseldefaulttemplateitem '.$childclass.'">';
		/*$html .= '<div class="w w5">'. getAxeFeaturedImg().'</div>';*/
		$url = get_the_permalink();
		$title = get_the_title();

		$heading = '<a href="'.$url.'" class="fullblockanchor" title="'.$title.'"></a><div class="galleryheading"><a href="'.$url.'"><h3>'.$title.'</h3></a></div>';
		$thumb = '<div class="galleryimg">'. themeaxe_getFeaturedImg().'</div>';
		$text = '<div class="galleryexcerpt">' . wpautop(get_the_excerpt()) . '</div>';

		$order['i'] = $thumb;
		$order['h'] = $heading;
		$order['c'] = $text;
		$content = '';
		foreach($order as $k => $v){
			$content .= $v;
		}
		$html .= $content;
		/*$html .= '<div class="galleryexcerpt w w7"><a href="'.$url.'"><h3>'.get_the_title().'</h3></a>'.wpautop(get_the_excerpt()).'</div>';*/
		$html .= '';
		$html .= '</div>';
	}
	return $html;
}

function themeaxe_gallerySliderDefaultTemplate($query,$childclass, $atts = null){
	$html = '';
	$order = themeaxe_getPredefinedLayout($atts['predefinedlayout']);
	while ( $query->have_posts() ) {
		$query->the_post();
		$html .= '<div class="axe_gallerycarouselitem axecarouselitem axe_gallerycarouseldefaulttemplateitem '.$childclass.'">';
		/*$html .= '<div class="w w12">'. getAxeFeaturedImg().'</div>';*/
		$url = get_the_permalink();
		$title = get_the_title();

		$heading = '<a href="'.$url.'" class="fullblockanchor" title="'.$title.'"></a><a href="'.$url.'"><h3>'.$title.'</h3></a>';
		$thumb = '<div class="w w12">'. themeaxe_getFeaturedImg().'</div>';
		$text = '<div class="galleryexcerpt w w12">'.wpautop(get_the_excerpt()).'</div>';

		$order['i'] = $thumb;
		$order['h'] = $heading;
		$order['c'] = $text;
		$content = '';
		foreach($order as $k => $v){
			$content .= $v;
		}

		$html .=$content;
		$html .= '';
		$html .= '</div>';
	}
	return $html;
}

function themeaxe_productSliderDefaultTemplate($query,$childclass, $atts = null){
	$html = '';
	$order = themeaxe_getPredefinedLayout($atts['predefinedlayout']);
	while ( $query->have_posts() ) {
		$query->the_post();
		$html .= '<div class="productcarouselitem axecarouselitem productcarouseldefaulttemplateitem '.$childclass.'">';
		$url = get_the_permalink();
		$title = get_the_title();

		$heading = '<a href="'.$url.'" class="fullblockanchor" title="'.$title.'"></a><a href="'.$url.'"><h3 class="axetransition">'.$title.'</h3></a>';
		$thumb = '<a href="'.$url.'">'. themeaxe_getFeaturedImg(0,'axetransition').'</a>';
		$text = '<p>'.get_the_excerpt().'</p>';

		$order['i'] = $thumb;
		$order['h'] = $heading;
		$order['c'] = $text;
		$content = '';
		foreach($order as $k => $v){
			$content .= $v;
		}

		$html .= '<div class="w w12">'.$content.'</div>';
		$html .= '';
		$html .= '</div>';
	}
	return $html;
}

function themeaxe_pageSliderDefaultTemplate($query,$childclass, $atts = null){
	$html = '';
	$order = themeaxe_getPredefinedLayout($atts['predefinedlayout']);

	while ( $query->have_posts() ) {
		$query->the_post();
		$thumb = themeaxe_getFeaturedImg(0,'axetransition');
		$html .= '<div class="pagecarouselitem axecarouselitem pagecarouseldefaulttemplateitem '.$childclass.'">';
		$url = get_the_permalink();
		$title = get_the_title();

		$heading = '<a href="'.$url.'" class="fullblockanchor" title="'.$title.'"></a><a href="'.$url.'"><h3 class="axetransition">'.$title.'</h3></a>';
		$thumb = '<a href="'.$url.'">'. $thumb.'</a>';
		$text = '<p>'.get_the_excerpt().'</p>';

		$order['i'] = $thumb;
		$order['h'] = $heading;
		$order['c'] = $text;
		$content = '';
		foreach($order as $k => $v){
			$content .= $v;
		}

		$html .= '<div class="w w12">'.$content.'</div>';
		$html .= '';
		$html .= '</div>';
	}
	return $html;
}

/* AXe Post Template */

function themeaxe_CarouselImageSliderHtml($query,$atts,$container,$childclass){
	$html ='';

	$html .= '<div id="bigimgtarget" class="axegallerymainimg fullcover">';
	/*$html .= '<img src="#" id="" />';*/
	$html .= themeaxe_PostSliderLoop($query,$atts,$container,$childclass);
	$html .= '';
	$html .= '</div>';

	return $html;
}

/* AXe Sorters */
function themeaxe_getSorters(){
	return array('date'=>'Date','menu_order'=>'Sort Order','title'=>'Title','rand'=>'Random');
}

function themeaxe_getSortersOptions($opt = ''){
	$html = '';
	$list = themeaxe_getSorters();
	foreach($list as $k => $v){
		$sel = '';
		if($opt == $k){
			$sel = 'selected="selected"';
		}
		$html .= '<option value="'.$k.'" '.$sel.'>'.$v.'</option>';
	}
	return $html;
}
/* AXe Sorters */

/* AXe Sort Direction */
function themeaxe_getSortDirection(){
	return array('desc'=>'Descending','asc'=>'Ascending');
}

function themeaxe_getSortDirectionOptions($opt = ''){
	$html = '';
	$list = themeaxe_getSortDirection();
	foreach($list as $k => $v){
		$sel = '';
		if($opt == $k){
			$sel = 'selected="selected"';
		}
		$html .= '<option value="'.$k.'" '.$sel.'>'.$v.'</option>';
	}
	return $html;
}
/* AXe Sort Direction */

/* AXe Post Category List */
function themeaxe_getPostCategoryList($tax = 'category'){
	$args = array(
		'orderby' => 'name',
		'order' => 'ASC',
		'taxonomy' => $tax,
	);
	return get_categories($args);
}

function themeaxe_getPostCategoryListOptions($args){
	$args = shortcode_atts(array(
		'opt' => '',
		'tax' => 'category',
		'all' => 1
	),$args);
	extract($args);

	$html = '';
	$list = themeaxe_getPostCategoryList($tax);
	if($all){
		$html .= '<option value="">All</option>';
	}
	$opt = explode(',',$opt);
	if(!isset($list['errors'])){
		foreach($list as $k){
			$sel = '';
			if(in_array($k->cat_ID,$opt)){
				$sel = 'selected="selected"';
			}
			$html .= '<option value="'.$k->cat_ID.'" '.$sel.'>'.$k->cat_name.'</option>';
		}
	}
	return $html;
}
/* AXe Post Category List */

/* getAxeYesNoOptions */
function themeaxe_getYesNoOptions($opt = 0){
	$html = '';
	$arr = array(0=>'No',1=>'Yes');
	foreach($arr as $k => $v){
		$sel = '';
		if($opt == $k){
			$sel = 'selected="selected"';
		}
		$html .= '<option value="'.$k.'" '.$sel.'>'.$v.'</option>';
	}
	return $html;
}
/* getAxeYesNoOptions */

/* AXe Get Posts */
function themeaxe_GetPosts($args = null){
	$args = shortcode_atts(array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'pagination' => false,
		'posts_per_page' => '5',
		'order' => 'DESC',
		'orderby' => 'date',
		'meta_key'=>null,
		'meta_value'=>null,
		'cat'=>0,
		'tax_query'=>null,
		'meta_query'=>null,
		'suppress_filters' => false,
		'paged'=>1
	),$args);
	return new WP_Query( $args );
}
/* AXe Get Posts */

/* AXe Get Pages */
function themeaxe_GetPageList($atts){

	$atts = shortcode_atts(
		array(
			'post__in'   => null,
			'post_type'  => 'page',
			'order' => 'ASC',
			'orderby' => 'ID',
		),
		$atts);


	$atts['post_type'] = 'page';
	$atts['posts_per_page'] = sizeof($atts['post__in']);

	return new WP_Query($atts);


}
/* AXe Get Pages */

function themeaxe_carousel_templates_defaults($list){
	array_push($list, 'default');
	return $list;
}
add_filter('axe_page_carousel_templates','themeaxe_carousel_templates_defaults',10,1);
add_filter('axe_post_carousel_templates','themeaxe_carousel_templates_defaults',10,1);
add_filter('axe_product_carousel_templates','themeaxe_carousel_templates_defaults',10,1);


/* As of ver 1.0.24 */
add_shortcode('scroller','themeaxe_scroller');
add_shortcode('scrolleritem','themeaxe_scrolleritem');
function themeaxe_scroller($atts, $content){
	$atts = shortcode_atts(
		array(
			'id' => 'scroller1',
			'mode'=>'horizontal',
			'in'=>800,
			'out'=>800,
			'time'=>8000,
			'autoplay'=>0,
			'ezing'=>'linear',
			'itemstoshow'=>1,
			'dir' => 'norm',
			'containerclass'=>'axescrollercontainer',
			'childclass'=>'axescrolleritem',
			'post_type'=>'logos',
			'playpausebutton' => 0
		),
		$atts);
	$container = $atts['containerclass'];
	$childclass = $atts['childclass'];
	$html = '';

	$html .= '<div id="'.$atts['id'].'" class="top'.$atts['post_type'].'-sidebar  top'.$atts['post_type'].'-carousel myclass axeCarousel '.$atts['post_type'].'axeCarousel mode'.$atts['mode'].'">';
	$html .= '<a href="#" class="axecarouselarrows axecarouselnext">&lt;</a>';
	if(intval($atts['playpausebutton']) == 1){
		$html .= '<a href="#" class="axecarouselarrows axecarouselplaypause fa fa-pause"></a>';
	}
	$html .= '<a href="#" class="axecarouselarrows axecarouselback">&gt;</a>';
	$html .= '<div class="ohidden">';
	$html .= '<div class="'.$container.' myinnerclass carouselcontainer">';

	$html .=  themeaxe_ContentFilterNoWPAutoP($content);

	$html .= '</div><!-- '.$container.' ends -->';
	$html .= '</div><!-- ohidden ends -->';
	$html .= '</div>';
	$html .= '<script> jQuery(document).ready(function($){ $(\'#'.$atts['id'].'\').axeCarousel({childcontainer:\''.$container.'\',childclass:\''.$childclass.'\',mode:\''.$atts['mode'].'\',i:'.$atts['in'].',t:'.$atts['time'].',o:'.$atts['out'].',autoplay:'.$atts['autoplay'].',dir:\''.$atts['dir'].'\',';
	if($atts['post_type'] == 'axe_gallery'){
		$html .= 'showbigimg:1,';
	}
	if(isset($atts['itemstoshow'])){
		$html .= 'toshow:'.intval($atts['itemstoshow']).',';
	}
	$html .= 'ezing:\''.$atts['ezing'].'\'}); }); </script>';

	return $html;
}
function themeaxe_scrolleritem($atts, $content){
	$atts = shortcode_atts(
		array(
			'id'=>'',
			'class'=>'',
			'title' =>'',
			'data' =>'',
			'rel' => ''
		),
		$atts);
	$class = $atts['class'];

	$data = trim($atts['data']);
	$data = $data ? 'data="' . $data . '" ' : '';

	$rel = trim($atts['rel']);
	$data .= $rel ? 'rel="' . $rel . '" ' : '';

	$html = '';
	$html .= '<div id="'.$atts['id'].'" '.$data.'class="defaultcarouselitem axecarouselitem defaultcarouseldefaulttemplateitem axescrolleritem '.$class.'">';
	$html .=  themeaxe_ContentFilterNoWPAutoP($content);
	$html .= '</div>';
	return $html;
}
/* As of ver 1.0.24 */
?>