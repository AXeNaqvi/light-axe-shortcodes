<?php
if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}
class ThemeAxeProductSliderWidget extends WP_Widget{

/**
	 * Sets up the widgets name etc
	 */
public function __construct() {
	parent::__construct(
			'themeaxe_productsliderwidget', // Base ID
			__('Light AXe ProductSlider Widget', 'light-axe'), // Name
			array( 'description' => __( 'Widget for the product slider.', 'light-axe' ), ) // Args
			);
}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}

		$cat  = $instance['cat'];
		$num  = intval($instance['num']);
		$toshow  = intval($instance['toshow']);
		$orderby  = $instance['orderby'];
		$dir  = $instance['dir'];
		$featured  = intval($instance['featured']);
		$template = $instance['template'];
		$predefinedlayout = $instance[ 'predefinedlayout' ];

		echo do_shortcode("[axeproductslider pcat='$cat' posts_per_page='$num' orderby='$orderby' order='$dir' featured='$featured' template='$template' predefinedlayout='$predefinedlayout' itemstoshow='$toshow']");

		/*echo do_shortcode('[axeproductslider]');*/

		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( '', 'light-axe' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' , 'light-axe'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
		if ( isset( $instance[ 'cat' ] ) ) {
			$cat = $instance[ 'cat' ];
		}
		else {
			$cat = __( '', 'light-axe');
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'cat' ); ?>"><?php _e( 'Category:' , 'light-axe'); ?></label>
			<select multiple class="widefat" id="<?php echo $this->get_field_id( 'cat' ); ?>" name="<?php echo $this->get_field_name( 'cat' ); ?>[]" value="<?php echo esc_attr( $cat ); ?>">
				<?php echo themeaxe_getPostCategoryListOptions(array('opt'=>$cat,'tax'=>'product_cat')); ?>
			</select>
		</p>
		<?php
		if ( isset( $instance[ 'num' ] ) ) {
			$num = $instance[ 'num' ];
		}
		else {
			$num = 5;
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'num' ); ?>"><?php _e( 'No. Of Posts:', 'light-axe' ); ?></label>
			<input type="number" class="widefat" id="<?php echo $this->get_field_id( 'num' ); ?>" name="<?php echo $this->get_field_name( 'num' ); ?>" value="<?php echo esc_attr( $num ); ?>" min="1" max="99"/>
		</p>
		<?php
		if ( isset( $instance[ 'toshow' ] ) ) {
			$toshow = $instance[ 'toshow' ];
		}
		else {
			$toshow = 3;
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'toshow' ); ?>"><?php _e( 'Items to show:' , 'light-axe'); ?></label>
			<input type="number" class="widefat" id="<?php echo $this->get_field_id( 'toshow' ); ?>" name="<?php echo $this->get_field_name( 'toshow' ); ?>" value="<?php echo esc_attr( $toshow ); ?>" min="1" max="99"/>
		</p>
		<?php
		if ( isset( $instance[ 'orderby' ] ) ) {
			$orderby = $instance[ 'orderby' ];
		}
		else {
			$orderby = __( 'date', 'light-axe');
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'orderby' ); ?>"><?php _e( 'Order By:', 'light-axe' ); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'orderby' ); ?>" name="<?php echo $this->get_field_name( 'orderby' ); ?>" value="<?php echo esc_attr( $orderby ); ?>">
				<?php echo themeaxe_getSortersOptions($orderby); ?>
			</select>
		</p>
		<?php
		if ( isset( $instance[ 'dir' ] ) ) {
			$dir = $instance[ 'dir' ];
		}
		else {
			$dir = __( 'desc', 'light-axe');
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'dir' ); ?>"><?php _e( 'Asc / Desc:' , 'light-axe'); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'dir' ); ?>" name="<?php echo $this->get_field_name( 'dir' ); ?>" value="<?php echo esc_attr( $dir ); ?>">
				<?php echo themeaxe_getSortDirectionOptions($dir); ?>
			</select>
		</p>
		<?php
		if ( isset( $instance[ 'featured' ] ) ) {
			$featured = $instance[ 'featured' ];
		}
		else {
			$featured = 0;
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'featured' ); ?>"><?php _e( 'Show Featured:' , 'light-axe'); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'featured' ); ?>" name="<?php echo $this->get_field_name( 'featured' ); ?>" value="<?php echo esc_attr( $featured ); ?>">
				<?php echo themeaxe_getYesNoOptions($featured); ?>
			</select>
		</p>
		<?php
		if ( isset( $instance[ 'template' ] ) ) {
			$template = $instance[ 'template' ];
		}
		else {
			$template = 'default';
		}
		$listtemplates = apply_filters('axe_product_carousel_templates',array());
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'template' ); ?>"><?php _e( 'Template:' , 'light-axe'); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'template' ); ?>" name="<?php echo $this->get_field_name( 'template' ); ?>">
				<?php
				if($listtemplates){
					foreach($listtemplates as $temp){
						$sel = '';
						if( $template  == $temp){
							$sel = 'selected="selected"';
						}
						?>
						<option value="<?php echo $temp; ?>" <?php echo $sel; ?>><?php echo ucwords($temp); ?> Template</option>
						<?php
					}
				}
				?>
			</select>
		</p>
		<?php
		if ( isset( $instance[ 'predefinedlayout' ] ) ) {
			$predefinedlayout = $instance[ 'predefinedlayout' ];
		}
		else {
			$predefinedlayout = 'ihc';
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'predefinedlayout' ); ?>"><?php _e( 'Predefined Default Layouts:', 'light-axe' ); ?></label><br/>
			<?php themeaxe_getAxePrdefinedLayouts($this->get_field_name( 'predefinedlayout' ),$predefinedlayout); ?>
		</p>
		<?php
		echo __('Product Slider Widget.', 'light-axe');
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$instance = array();

		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['cat'] = ( ! is_null( $new_instance['cat'] ) ) ? implode(',', $new_instance['cat'] ) : '';
		$instance['num'] = ( ! empty( $new_instance['num'] ) ) ? intval(strip_tags( $new_instance['num'] )) : 5;
		$instance['toshow'] = ( ! empty( $new_instance['toshow'] ) ) ? strip_tags( $new_instance['toshow'] ) : 3;
		$instance['orderby'] = ( ! empty( $new_instance['orderby'] ) ) ? strip_tags( $new_instance['orderby'] ) : 'date';
		$instance['dir'] = ( ! empty( $new_instance['dir'] ) ) ? strip_tags( $new_instance['dir'] ) : 'desc';
		$instance['featured'] = ( ! empty( $new_instance['featured'] ) ) ? intval(strip_tags( $new_instance['featured'] )) : 0;
		$instance['template'] =( ! empty( $new_instance['template'] ) ) ? strip_tags( $new_instance['template'] ) : 'default';
		$instance['predefinedlayout'] =( ! empty( $new_instance['predefinedlayout'] ) ) ? strip_tags( $new_instance['predefinedlayout'] ) : 'ihc';

		return $instance;

	}

}

add_action( 'widgets_init', function(){
	register_widget( 'ThemeAxeProductSliderWidget' );
});

?>