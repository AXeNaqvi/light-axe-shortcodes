<?php
if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}
class ThemeAxeAdBoxWidget extends WP_Widget{

/**
	 * Sets up the widgets name etc
	 */
public function __construct() {
	parent::__construct(
			'themeaxe_adboxwidget', // Base ID
			__('Light AXe AdBox Widget', 'light-axe'), // Name
			array( 'description' => __( 'Widget for the product slider.', 'light-axe' ), ) // Args
		);
}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget
		echo $args['before_widget'];
		/*if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}*/
		$vals = array();
		$vals['title'] = apply_filters( 'widget_title',$instance['title']);
		$vals['subtitle'] = apply_filters( 'widget_title',$instance['subtitle']);
		$vals['img'] = $instance['image'];
		$vals['url'] = $instance['link'];
		$vals['newwindow'] = $instance['innewtab'];
		$vals['class'] = $instance['classes'];
		$vals['readmore'] = $instance['readmore'];
		$vals['text'] = apply_filters( 'widget_content', htmlentities($instance['text'],ENT_QUOTES));
		$vals['readmoretext'] = apply_filters( 'widget_title',$instance['readmoretext']);
		$vals['predefinedlayout'] = $instance[ 'predefinedlayout' ];

		/*echo do_shortcode("[axeadbox title='$title' subtitle='$subtitle' url='$url' img='$img' class='$class' newwindow='$newwin' readmore='$readmore' text='$text' readmoretext='$readmoretext' predefinedlayout='$predefinedlayout']");*/

		echo themeaxe_AdBox($vals);

		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin
		$title = isset($instance['title']) ? $instance['title'] : '';
		$subtitle = isset($instance['subtitle']) ? $instance['subtitle'] : '';
		$image = isset($instance['image']) ? $instance['image'] : '';
		$link = isset($instance['link']) ? $instance['link'] : '';
		$text = isset($instance['text']) ? $instance['text'] : '';
		$readmoretext = isset($instance['readmoretext']) ? $instance['readmoretext'] : '';
		$innewtab = isset($instance['innewtab']) ? intval($instance['innewtab']) : 0;
		$readmore = isset($instance['readmore']) ? intval($instance['readmore']) : 0;
		$class = isset($instance['classes']) ? trim($instance['classes']) : '';
		$predefinedlayout = isset($instance['predefinedlayout']) ? $instance['predefinedlayout'] : 'ihc';
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'light-axe' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'subtitle' ); ?>"><?php _e( 'Sub-title:', 'light-axe' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'subtitle' ); ?>" name="<?php echo $this->get_field_name( 'subtitle' ); ?>" type="text" value="<?php echo esc_attr( $subtitle ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'image' ); ?>"><?php _e( 'Image:', 'light-axe' ); ?></label>
			<input class="widefat themeaxemedialoader" id="<?php echo $this->get_field_id( 'image' ); ?>" name="<?php echo $this->get_field_name( 'image' ); ?>" type="text" value="<?php echo esc_attr( $image ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'URL:', 'light-axe' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" type="url" value="<?php echo esc_attr( $link ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Excerpt:', 'light-axe' ); ?></label>
			<textarea class="widefat" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>"><?php echo esc_textarea( htmlspecialchars_decode($text )); ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'readmoretext' ); ?>"><?php _e( 'Read More Text:', 'light-axe' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'readmoretext' ); ?>" name="<?php echo $this->get_field_name( 'readmoretext' ); ?>" type="text" value="<?php echo esc_attr( $readmoretext ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'innewtab' ); ?>"><?php _e( 'In New Tab ?', 'light-axe' ); ?></label>
			<input type="checkbox" class="" id="<?php echo $this->get_field_id( 'innewtab' ); ?>" name="<?php echo $this->get_field_name( 'innewtab' ); ?>" value="1" <?php if($innewtab){echo 'checked="checked"';} ?>>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'readmore' ); ?>"><?php _e( 'Add Read More ?', 'light-axe' ); ?></label>
			<input type="checkbox" class="" id="<?php echo $this->get_field_id( 'readmore' ); ?>" name="<?php echo $this->get_field_name( 'readmore' ); ?>" value="1" <?php if($readmore){echo 'checked="checked"';} ?>>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'classes' ); ?>"><?php _e( 'Custom Class(es)' , 'light-axe'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'classes' ); ?>" name="<?php echo $this->get_field_name( 'classes' ); ?>" type="text" value="<?php echo esc_attr( $class ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'predefinedlayout' ); ?>"><?php _e( 'Predefined Default Layouts:', 'light-axe' ); ?></label><br/>
			<?php themeaxe_getAxePrdefinedLayouts($this->get_field_name( 'predefinedlayout' ),$predefinedlayout); ?>
		</p>
		<?php
		echo __('Ad Box Widget.', 'light-axe');
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['subtitle'] = ( ! empty( $new_instance['subtitle'] ) ) ? strip_tags( $new_instance['subtitle'] ) : '';
		$instance['image'] = ( ! empty( $new_instance['image'] ) ) ? strip_tags( $new_instance['image'] ) : '';
		$instance['link'] = ( ! empty( $new_instance['link'] ) ) ? strip_tags( $new_instance['link'] ) : '';
		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? wp_kses_post(str_replace("\r\n","\n",$new_instance['text'] )) : '';
		$instance['innewtab'] = ( ! empty( $new_instance['innewtab'] ) ) ? intval( $new_instance['innewtab'] ) : 0;
		$instance['readmore'] = ( ! empty( $new_instance['readmore'] ) ) ? intval( $new_instance['readmore'] ) : 0;
		$instance['readmoretext'] = ( ! empty( $new_instance['readmoretext'] ) ) ? strip_tags( $new_instance['readmoretext'] ) : 'Read More';
		$instance['classes'] = ( ! empty( $new_instance['classes'] ) ) ? strip_tags( $new_instance['classes'] ) : '';
		$instance['predefinedlayout'] =( ! empty( $new_instance['predefinedlayout'] ) ) ? strip_tags( $new_instance['predefinedlayout'] ) : 'ihc';

		return $instance;

	}

}

add_action( 'widgets_init', function(){
	register_widget( 'ThemeAxeAdBoxWidget' );
});

?>