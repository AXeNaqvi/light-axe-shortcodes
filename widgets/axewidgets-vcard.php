<?php
if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}
class ThemeAxeVCardWidget extends WP_Widget{

	private $allfields;
	private $allfieldtypes;
	private $allfieldlabels;
	public function __construct() {
		parent::__construct(
			'themeaxe_vcardwidget', // Base ID
			__('Light AXe VCard Widget', 'light-axe'), // Name
			array( 'description' => __( 'Widget for the visting card.', 'light-axe' ), ) // Args
		);
		$this->allfields = apply_filters('filter_vcardwidget_allfields',array('address','phone1','phone2','fax','email'));
		$this->allfieldtypes = apply_filters('filter_vcardwidget_fieldtypes',array('address'=>'text','address2'=>'text','phone1'=>'text','phone2'=>'text','fax'=>'text','email'=>'email'));
		$this->allfieldlabels = apply_filters('filter_vcardwidget_fields',array('address'=>__('Address', 'light-axe'),'phone1'=>__('Phone 1', 'light-axe'),'phone2'=>__('Phone 2', 'light-axe'),'fax'=>__('Fax', 'light-axe'),'email'=>__('Email', 'light-axe')));
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		$showthese = isset( $instance[ 'showthese' ] ) ? implode(',',$instance[ 'showthese' ]) : '';
		$usedefaults = isset( $instance[ 'usedefaults' ] ) ? intval($instance[ 'usedefaults' ]) : 0;
		$showarray = explode(',',$showthese);
		$code = '[axevcard showthese=\''.$showthese.'\'  usedefaults=\''.$usedefaults.'\'';
		$fieldstype = $this->allfields;
		$address2 = isset( $instance[ 'address2' ] ) ? $instance['address2'] : '';
		foreach($fieldstype as $fk){
			if(in_array($fk, $showarray)){
				$$fk =  isset( $instance[ $fk ] )  && !empty( $instance[ $fk ] ) ? $instance[ $fk ] : '';
				if($fk == 'address'){
					$$fk .= '<br/>' . $address2;
				}
				$code .= ' '.$fk.'=\''.$$fk.'\'';
			}
		}
		$code .= ']';
		echo do_shortcode($code);

		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		global $axethemesettings;
		$addressObj = $axethemesettings->contact;

		$title = isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : __( '', 'light-axe' );
		$showthese = isset( $instance[ 'showthese' ] ) ? $instance[ 'showthese' ] : array();
		$usedefaults = isset( $instance[ 'usedefaults' ] ) ? intval($instance[ 'usedefaults' ]) : 0;
		$udchecked = intval($usedefaults) ? 'checked="checked"' : '';
		$allfields = isset( $instance[ 'allfields' ] ) ? $instance[ 'allfields' ] : $this->allfields;

		$fieldstype = $this->allfieldtypes;
		foreach($fieldstype as $fk => $fv){
			$$fk =  isset( $instance[ $fk ] )  && !empty( $instance[ $fk ] ) ? $instance[ $fk ] : $addressObj->$fk->value;
		}

		$fields = $this->allfieldlabels;
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' , 'light-axe'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<div class="axethemeadminwrapper">
			<div class="axethemetoggle activeaxethemetoggle">
				<div class="inside">
					<table>
						<thead>
							<tr>
								<th width="10">&nbsp</th>
								<th>Field</th>
								<th width="20">Show</th>
							</tr>
						</thead>
						<tbody class="axesortme">
							<?php
							$ix = 0;
							foreach($allfields as $field){
								$checked = in_array($field,$showthese) ? 'checked="checked"' : '';
								?>
								<tr class="axesortable ui-sortable">
									<td align="center" class="axesorthandle">=</td>
									<td>
										<input type="<?php echo $fieldstype[$field]; ?>" placeholder="<?php echo $fields[$field]; ?>" name="<?php echo $this->get_field_name( $field); ?>" value="<?php echo $$field; ?>" />
										<?php
										if($field == 'address'){
											?>
											<input type="text" placeholder="Address 2" name="<?php echo $this->get_field_name( 'address2'); ?>" value="<?php echo $address2; ?>" />
											<?php
										}
										?>

										<input type="hidden" name="<?php echo $this->get_field_name( 'allfields[]' ); ?>" value="<?php echo $field; ?>" />
									</td>
									<td align="center"><input type="checkbox" name="<?php echo $this->get_field_name( 'showthese[]' ); ?>" <?php echo $checked; ?> value="<?php echo $field; ?>" /></td>
								</tr>
								<?php
								$ix++;
							}
							?>
							<tr><td></td><td><?php _e('Do not override default values with the above ones.', 'light-axe');?></td><td><input type="checkbox" name="<?php echo $this->get_field_name( 'usedefaults' ); ?>" <?php echo $udchecked; ?> value="1" /></td></tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<script>
			(function($){
				$(document).ready(function(){
					$('.axesortme').sortable({'handle':'.axesorthandle'});
				});
			})(jQuery);
		</script>
		<?php
		echo __('VCard: Set Them Up Under Light AXe Settings.', 'light-axe');
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		$fieldstype = $this->allfieldtypes;
		foreach($fieldstype as $fk => $fv){
			$instance[$fk] = ( ! empty( $new_instance[$fk] ) ) ? strip_tags( $new_instance[$fk] ) : '';
		}
		$instance['allfields'] = isset($new_instance['allfields']) ? $new_instance['allfields'] : array();

		$instance['showthese'] = isset($new_instance['showthese']) ? $new_instance['showthese'] : array();
		$instance['usedefaults'] = isset($new_instance['usedefaults']) ? $new_instance['usedefaults'] : 0;

		return $instance;

	}

}

add_action( 'widgets_init', function(){
	register_widget( 'ThemeAxeVCardWidget' );
});

?>