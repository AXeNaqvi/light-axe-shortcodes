<?php
if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}
class ThemeAxeGMapWidget extends WP_Widget{

/**
	 * Sets up the widgets name etc
	 */
public function __construct() {
	parent::__construct(
			'themeaxe_gmapwidget', // Base ID
			__('Light AXe GMaps Widget', 'light-axe'), // Name
			array( 'description' => __( 'Widget for the GMap.', 'light-axe' ), ) // Args
		);
}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		$address = (!empty($instance['address']) )? urldecode($instance['address']) : themeaxe_getDefaultAddress();
		$width = (!empty($instance['width']) )? $instance['width'] : '100%';
		$height = (!empty($instance['height'])) ? $instance['height'] : '250';
		$type = isset($instance['type']) ? $instance['type'] : 'api';

		if($type == 'iframe'){
			echo do_shortcode('[embedmap src="'.$address.'" width="'.$width.'" height="'.$height.'"]');
		}else{
			echo do_shortcode('[axeGmap location="'.$address.'" width="'.$width.'" height="'.$height.'"]');
		}
		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin
		$title = isset($instance['title']) ? $instance['title'] : '';
		$width = isset($instance['width']) ? $instance['width'] : '100%';
		$height = isset($instance['height']) ? $instance['height'] : '250';
		$address = isset($instance['address']) ? $instance['address'] : themeaxe_getDefaultAddress();
		$type = isset($instance['type']) ? $instance['type'] : 'api';

		$gmaptypes = array('api'=>__('GMap EMbed API', 'light-axe'),'iframe'=>__('Iframe Based Map', 'light-axe'));
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'light-axe' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'address' ); ?>"><?php _e( 'Address:', 'light-axe' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'address' ); ?>" name="<?php echo $this->get_field_name( 'address' ); ?>" type="text" value="<?php echo esc_attr( $address ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'width' ); ?>"><?php _e( 'Width:', 'light-axe' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'width' ); ?>" name="<?php echo $this->get_field_name( 'width' ); ?>" type="text" value="<?php echo esc_attr( $width ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'height' ); ?>"><?php _e( 'Height:', 'light-axe' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'height' ); ?>" name="<?php echo $this->get_field_name( 'height' ); ?>" type="text" value="<?php echo esc_attr( $height ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'type' ); ?>"><?php _e( 'Map Type:', 'light-axe' ); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'type' ); ?>" name="<?php echo $this->get_field_name( 'type' ); ?>">
				<?php
				foreach($gmaptypes as $gk=>$gv){
					$sel = $gk == $type ? 'selected="selected"' : '';
					?>
					<option value="<?php echo $gk; ?>" <?php echo $sel;?>><?php echo $gv; ?></option>
				<?php } ?>
			</select>
		</p>
		<?php
		echo __('GMap: Set Location Under Light AXe Settings.', 'light-axe');
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['width'] = ( ! empty( $new_instance['width'] ) ) ? strip_tags( $new_instance['width'] ) : '100%';
		$instance['height'] = ( ! empty( $new_instance['height'] ) ) ? strip_tags( $new_instance['height'] ) : '250';
		$instance['address'] = ( ! empty( $new_instance['address'] ) ) ? strip_tags( $new_instance['address'] ) : '';
		$instance['type'] = ( ! empty( $new_instance['type'] ) ) ? strip_tags( $new_instance['type'] ) : 'api';

		return $instance;

	}

}

add_action( 'widgets_init', function(){
	register_widget( 'ThemeAxeGMapWidget' );
});

?>