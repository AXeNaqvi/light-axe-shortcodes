<?php
if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}
class ThemeAxePageSliderWidget extends WP_Widget{

/**
	 * Sets up the widgets name etc
	 */
public function __construct() {
	parent::__construct(
			'themeaxe_pagesliderwidget', // Base ID
			__('Light AXe PageSlider Widget', 'light-axe'), // Name
			array( 'description' => __( 'Widget for the page slider.', 'light-axe' ), ) // Args
			);
}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget

		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}

		$pageids  = $instance['pageids'];
		$toshow  = intval($instance['toshow']);
		$template = $instance['template'];
		$predefinedlayout = $instance[ 'predefinedlayout' ];

		/*echo do_shortcode("[doublebar]");*/

		echo do_shortcode("[axepageslider pageids='$pageids' template='$template' predefinedlayout='$predefinedlayout' itemstoshow='$toshow']");

		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( '', 'light-axe' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'light-axe' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
		if ( isset( $instance[ 'pageids' ] ) ) {
			$pageids = $instance[ 'pageids' ];
		}
		else {
			$pageids = __( '', 'light-axe');
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'pageids' ); ?>"><?php _e( 'Page IDs:', 'light-axe' ); ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'pageids' ); ?>" name="<?php echo $this->get_field_name( 'pageids' ); ?>" value="<?php echo esc_attr( $pageids ); ?>" />
		</p>
		<?php
		if ( isset( $instance[ 'toshow' ] ) ) {
			$toshow = $instance[ 'toshow' ];
		}
		else {
			$toshow = 3;
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'toshow' ); ?>"><?php _e( 'Items to show:', 'light-axe' ); ?></label>
			<input type="number" class="widefat" id="<?php echo $this->get_field_id( 'toshow' ); ?>" name="<?php echo $this->get_field_name( 'toshow' ); ?>" value="<?php echo esc_attr( $toshow ); ?>" min="1" max="99"/>
		</p>
		<?php
		if ( isset( $instance[ 'template' ] ) ) {
			$template = $instance[ 'template' ];
		}
		else {
			$template = 'default';
		}
		$listtemplates = apply_filters('axe_page_carousel_templates',array());
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'template' ); ?>"><?php _e( 'Template:', 'light-axe' ); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'template' ); ?>" name="<?php echo $this->get_field_name( 'template' ); ?>">
				<?php
				if($listtemplates){
					foreach($listtemplates as $temp){
						$sel = '';
						if( $template  == $temp){
							$sel = 'selected="selected"';
						}
						?>
						<option value="<?php echo $temp; ?>" <?php echo $sel; ?>><?php echo ucwords($temp); ?> Template</option>
						<?php
					}
				}
				?>
			</select>
		</p>
		<?php
		if ( isset( $instance[ 'predefinedlayout' ] ) ) {
			$predefinedlayout = $instance[ 'predefinedlayout' ];
		}
		else {
			$predefinedlayout = 'ihc';
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'predefinedlayout' ); ?>"><?php _e( 'Predefined Default Layouts:' , 'light-axe'); ?></label><br/>
			<?php themeaxe_getAxePrdefinedLayouts($this->get_field_name( 'predefinedlayout' ),$predefinedlayout); ?>
		</p>


		<?php
		echo __('Page Slider Widget.', 'light-axe');
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['pageids'] =( ! empty( $new_instance['pageids'] ) ) ? strip_tags( $new_instance['pageids'] ) : '';
		$instance['toshow'] = ( ! empty( $new_instance['toshow'] ) ) ? strip_tags( $new_instance['toshow'] ) : 3;
		$instance['template'] =( ! empty( $new_instance['template'] ) ) ? strip_tags( $new_instance['template'] ) : 'default';
		$instance['predefinedlayout'] =( ! empty( $new_instance['predefinedlayout'] ) ) ? strip_tags( $new_instance['predefinedlayout'] ) : 'ihc';

		return $instance;

	}

}

add_action( 'widgets_init', function(){
	register_widget( 'ThemeAxePageSliderWidget' );
});

?>