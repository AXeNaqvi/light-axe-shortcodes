<?php
if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}

$listWidgets = array('social','vcard','gmap');
foreach ($listWidgets as $key) {
	include_once THEMEAXEROOT.'/widgets/widget-'.$key.'.php';
}

function themeaxe_get_dynamic_sidebar($index = 1)
{
	$sidebar_contents = "";
	ob_start();
	dynamic_sidebar($index);
	$sidebar_contents = ob_get_clean();
	return $sidebar_contents;
}

function themeaxe_get_sidebar($name='',$calldefault = false){
	if ( is_active_sidebar( 'sidebar-'.$name ) ) {
		return themeaxe_get_dynamic_sidebar( 'sidebar-'.$name );
	}else if($calldefault){
		return themeaxe_get_dynamic_sidebar( 'sidebar-default' );
	}
}


function themeaxe_sidebar($name='',$calldefault = false){
	if ( is_active_sidebar( 'sidebar-'.$name ) ) {
		dynamic_sidebar( 'sidebar-'.$name );
	}else if($calldefault){
		dynamic_sidebar( 'sidebar-default' );
	}
}

?>