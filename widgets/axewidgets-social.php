<?php
if(!defined('THEMEAXESHORTCODES')){
	exit('What are you doing here??');
}
class ThemeAxeSocialWidget extends WP_Widget{

/**
	 * Sets up the widgets name etc
	 */
public function __construct() {
	parent::__construct(
			'themeaxe_socialwidget', // Base ID
			__('Light AXe Social Widget', 'light-axe'), // Name
			array( 'description' => __( 'Widget for the social media links.', 'light-axe' ), ) // Args
			);
}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}
		echo do_shortcode('[axeSocial]');

		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( '', 'light-axe' );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'light-axe' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
		echo __('Social Media Links: Set Them Up Under Light AXe Settings.', 'light-axe');
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;

	}

}

add_action( 'widgets_init', function(){
	register_widget( 'ThemeAxeSocialWidget' );
});

?>