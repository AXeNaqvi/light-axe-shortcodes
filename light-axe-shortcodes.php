<?php
/**
 * Plugin Name: Light AXe Shortcodes
 * Plugin URI: https://bitbucket.org/AXeNaqvi/light-axe-shortcodes
 * Description: Adds Light AXe's shortcodes
 * Author: AXe
 * Author URI: https://bitbucket.org/AXeNaqvi
 * Version: 1.0.6
 *
 * Copyright: (c) 2019 AXe (axe.naqvi@gmail.com)
 *
 * License: GNU General Public License v2.0 and above
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit;

define('THEMEAXESHORTCODES','light-axe-shortcodes');

add_action('after_setup_theme','themeaxe_init_shortcodes',999);

function themeaxe_init_shortcodes(){
	if(defined('THEMEAXE')){
		themeaxe_init_axe_shortcodes();
		themeaxe_init_axe_widgets();
	}
}

function themeaxe_init_axe_shortcodes(){
	$listShortcodes = array(
		'sliders',
		'boxes',
		'registerlogin',
		'woocommerce',
		'map',
		'vcard',
		'video',
		'sociallinks',
		'button',
		'copyrightyear',
	);
	foreach ($listShortcodes as $key) {
		require_once plugin_dir_path( __FILE__ ).'shortcodes/axeshortcodes-' . $key . '.php';
	}
}

function themeaxe_init_axe_widgets(){
	$listWidgets = array(
		'social',
		'vcard',
		'gmap',
		'postslider',
		'pageslider',
		'productslider',
		'adbox',
	);
	foreach ($listWidgets as $key) {
		require_once plugin_dir_path( __FILE__ ).'widgets/axewidgets-' . $key . '.php';
	}
}